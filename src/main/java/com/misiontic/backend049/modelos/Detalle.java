package com.misiontic.backend049.modelos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="detalles")
public class Detalle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idDetalle;

    @Column(name="cantidaddetalle", nullable = false)
    private double cantidadDetalle;

    @Column(name="valordetalle", nullable = false)
    private double valorDetalle;

    @Column(name="totaldetalle", nullable = false)
    private double totalDetalle;

    @ManyToOne
    @JoinColumn(name="idProducto")
    private Producto producto;

    @ManyToOne
    @JoinColumn(name="idTransaccion")
    private Transaccion transaccion;

    public Detalle() {
    }

    public Detalle(double cantidadDetalle, double valorDetalle, double totalDetalle) {
        this.cantidadDetalle = cantidadDetalle;
        this.valorDetalle = valorDetalle;
        this.totalDetalle = totalDetalle;
    }

    public long getIdDetalle() {
        return idDetalle;
    }

    public void setIdDetalle(long idDetalle) {
        this.idDetalle = idDetalle;
    }

    public double getCantidadDetalle() {
        return cantidadDetalle;
    }

    public void setCantidadDetalle(double cantidadDetalle) {
        this.cantidadDetalle = cantidadDetalle;
    }

    public double getValorDetalle() {
        return valorDetalle;
    }

    public void setValorDetalle(double valorDetalle) {
        this.valorDetalle = valorDetalle;
    }

    public double getTotalDetalle() {
        return totalDetalle;
    }

    public void setTotalDetalle(double totalDetalle) {
        this.totalDetalle = totalDetalle;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Transaccion getTransaccion() {
        return transaccion;
    }

    public void setTransaccion(Transaccion transaccion) {
        this.transaccion = transaccion;
    }

    @Override
    public String toString() {
        return "Detalle [cantidadDetalle=" + cantidadDetalle + ", idDetalle=" + idDetalle + ", producto=" + producto
                + ", totalDetalle=" + totalDetalle + ", transaccion=" + transaccion + ", valorDetalle=" + valorDetalle
                + "]";
    }

    
    



}
