package com.misiontic.backend049.modelos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "roles")
public class Roles {
    

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idRol;


    @Column(name="nombrerol")
    private String nombreRol;


    public Roles() {
    }


    public Roles(String nombreRol) {
        this.nombreRol = nombreRol;
    }


    public long getIdRol() {
        return idRol;
    }


    public void setIdRol(long idRol) {
        this.idRol = idRol;
    }


    public String getNombreRol() {
        return nombreRol;
    }


    public void setNombreRol(String nombreRol) {
        this.nombreRol = nombreRol;
    }


    @Override
    public String toString() {
        return "Roles [idRol=" + idRol + ", nombreRol=" + nombreRol + "]";
    }


  
    
    
}
