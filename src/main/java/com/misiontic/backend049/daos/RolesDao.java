package com.misiontic.backend049.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.misiontic.backend049.modelos.Roles;

public interface RolesDao extends JpaRepository<Roles, Long>{
    
}
