package com.misiontic.backend049.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.misiontic.backend049.modelos.Usuario;

public interface UsuarioDao extends JpaRepository<Usuario, Long>{
    
    public Usuario findByUserName(String userName);
}
