package com.misiontic.backend049.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.misiontic.backend049.modelos.Producto;

public interface ProductoDao extends JpaRepository<Producto, Long> {

   
}
