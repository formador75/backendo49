package com.misiontic.backend049.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.misiontic.backend049.modelos.Detalle;

public interface DetalleDao  extends JpaRepository<Detalle, Long> {
    
}
