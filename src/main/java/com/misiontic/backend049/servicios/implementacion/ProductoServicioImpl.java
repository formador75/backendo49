package com.misiontic.backend049.servicios.implementacion;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.misiontic.backend049.daos.ProductoDao;
import com.misiontic.backend049.modelos.Producto;
import com.misiontic.backend049.servicios.ServicioProducto;

public class ProductoServicioImpl implements ServicioProducto {
    

    @Autowired
    ProductoDao productoDao;

    public Producto save(Producto producto){

        Producto nuevoProducto = productoDao.save(new Producto(producto.getNombreProducto(), producto.getPrecioCompra(), producto.getPrecioVenta(), producto.getCantidad()));
        return nuevoProducto;

    }


    public List<Producto> listaTodos(){

        List<Producto> productos = new ArrayList<Producto>();
        productoDao.findAll().forEach(productos::add);

        if(!productos.isEmpty()){
      
        return  productos;
        }else{
            return  productos;
        }


        
    }

    
}
