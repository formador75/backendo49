package com.misiontic.backend049.servicios;

import java.util.List;

import com.misiontic.backend049.modelos.Producto;

public interface ServicioProducto {

    public Producto save(Producto producto);

    public List<Producto> listaTodos();
}
