package com.misiontic.backend049.controladores;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.misiontic.backend049.daos.DetalleDao;
import com.misiontic.backend049.modelos.Detalle;

@RestController
@RequestMapping("/api")
public class ControladorDetalle {
    
    @Autowired
    DetalleDao detalleDao;

    @PostMapping("/detalles")
    public ResponseEntity<Detalle> guardarDetalle(@RequestBody Detalle detalle){

        Detalle nuevaDetalle = detalleDao.save(detalle);
        return new ResponseEntity<>(nuevaDetalle, HttpStatus.CREATED);


    }

    @PutMapping("/detalles/{id}")
    public ResponseEntity<Detalle> actualizarDetalle(@PathVariable("id") long id, @RequestBody Detalle detalle){

        Optional<Detalle> detalleactualizar = detalleDao.findById(id);
        if(detalleactualizar.isPresent()){
             
            Detalle _detalle = detalleactualizar.get();
            
            _detalle.setCantidadDetalle(detalle.getCantidadDetalle());
            _detalle.setValorDetalle(detalle.getValorDetalle());
            _detalle.setTotalDetalle(detalle.getTotalDetalle());
            _detalle.setTransaccion(detalle.getTransaccion());
            _detalle.setProducto(detalle.getProducto());

            return new ResponseEntity<>(detalleDao.save(_detalle), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @GetMapping("/detalles")
    public ResponseEntity<List<Detalle>> listarDetalles(){

        List<Detalle> detalle = new ArrayList<Detalle>();
        detalleDao.findAll().forEach(detalle::add);

        if(detalle.isEmpty()){
        return  new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }else{
        return  new ResponseEntity<>(detalle, HttpStatus.OK);
        }

    }



    @GetMapping("/detalles/{id}")
    public ResponseEntity<Detalle> listarDetalle(@PathVariable("id") long id){

        Optional<Detalle> detalle = detalleDao.findById(id);
        
        if(detalle.isPresent()){

            return new ResponseEntity<>(detalle.get(), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }




    @DeleteMapping("/detalles/{id}")
    public ResponseEntity<HttpStatus> borrarDetalle(@PathVariable("id") long id){

        detalleDao.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }


}

