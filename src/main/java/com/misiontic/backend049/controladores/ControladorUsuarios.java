package com.misiontic.backend049.controladores;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.misiontic.backend049.daos.UsuarioDao;
import com.misiontic.backend049.modelos.Usuario;

@CrossOrigin("*")
@RestController
@RequestMapping("/api")
public class ControladorUsuarios {
    
    @Autowired
    UsuarioDao usuarioDao;

    @PostMapping("/login")
    public ResponseEntity<Usuario> login(@RequestParam String nombre, @RequestParam String clave){
        Usuario usuario = usuarioDao.findByUserName(nombre);
        if(clave.equals(usuario.getClave())){
            return new ResponseEntity<>(usuario, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/usuarios/{id}")
    public ResponseEntity<Usuario> actualizarUsuario(@PathVariable long id, @RequestBody Usuario usuario){

        Optional<Usuario> _usuario = usuarioDao.findById(id);

        if(_usuario.isPresent()){
            Usuario usuarioActualizar = _usuario.get();
            usuarioActualizar.setClave(usuario.getClave());
            usuarioActualizar.setEmail(usuario.getEmail());
            usuarioActualizar.setUserName(usuario.getUserName());
            usuarioActualizar.setRoles(usuario.getRoles());

            return new ResponseEntity<>(usuarioDao.save(usuarioActualizar), HttpStatus.OK);


        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }




    }


    @PostMapping("/usuarios")
    public ResponseEntity<Usuario> guardarUsuario(@RequestBody Usuario usuario){

        Usuario nuevoUsuario = usuarioDao.save(usuario);
        return new ResponseEntity<>(nuevoUsuario, HttpStatus.CREATED);

        
    }

    @GetMapping("/usuarios/{id}")
    public ResponseEntity<Usuario> listarUsuario(@PathVariable long id){

        Optional<Usuario> usuario = usuarioDao.findById(id);
        
        if(usuario.isPresent()){
            return new ResponseEntity<>(usuario.get(), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        }


    }

    @GetMapping("/usuarios")
    public ResponseEntity<List<Usuario>> listarUsuarios(){

        List<Usuario> usuarios = new ArrayList<Usuario>();
        usuarioDao.findAll().forEach(usuarios::add);

        if(usuarios.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }else{
            return new ResponseEntity<>(usuarios, HttpStatus.OK);

        }        
    }





    @DeleteMapping("/usuarios/{id}")
    public ResponseEntity<HttpStatus> borrarUsuario(@PathVariable long id){

        usuarioDao.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);


    }


}
