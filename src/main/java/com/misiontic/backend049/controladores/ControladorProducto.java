package com.misiontic.backend049.controladores;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.misiontic.backend049.daos.ProductoDao;
import com.misiontic.backend049.modelos.Producto;
import com.misiontic.backend049.servicios.implementacion.ProductoServicioImpl;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ControladorProducto {
    
    @Autowired
    ProductoDao productoDao;

    
    @PostMapping("/productos")
    public ResponseEntity<Producto> guardarProducto(@RequestBody Producto producto){

        Producto nuevoProducto = productoDao.save(new Producto(producto.getNombreProducto(), producto.getPrecioCompra(), producto.getPrecioVenta(), producto.getCantidad()));
        return new ResponseEntity<>(nuevoProducto, HttpStatus.CREATED);

    }


    @PutMapping("/productos/{id}")
    public ResponseEntity<Producto> actualizarProducto(@PathVariable("id") long id, @RequestBody Producto producto){

        Optional<Producto> productoactualizar = productoDao.findById(id);
        if(productoactualizar.isPresent()){
             
            Producto _producto = productoactualizar.get();
            
            _producto.setCantidad(producto.getCantidad());
            _producto.setNombreProducto(producto.getNombreProducto());
            _producto.setPrecioCompra(producto.getPrecioCompra());
            _producto.setPrecioVenta(producto.getPrecioVenta());

            return new ResponseEntity<>(productoDao.save(_producto), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @GetMapping("/productos/{id}")
    public ResponseEntity<Producto> listarProducto(@PathVariable("id") long id){

        Optional<Producto> producto = productoDao.findById(id);
        
        if(producto.isPresent()){

            return new ResponseEntity<>(producto.get(), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @GetMapping("/productos")
    public ResponseEntity<List<Producto>> listarProductos(){

        List<Producto> productos = new ArrayList<Producto>();
        productoDao.findAll().forEach(productos::add);

        if(productos.isEmpty()){
        return  new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }else{
        return  new ResponseEntity<>(productos, HttpStatus.OK);
        }

    }


    @DeleteMapping("/productos/{id}")
    public ResponseEntity<HttpStatus> borrarProducto(@PathVariable("id") long id){

        productoDao.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }
}
