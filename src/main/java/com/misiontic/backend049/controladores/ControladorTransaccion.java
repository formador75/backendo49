package com.misiontic.backend049.controladores;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.misiontic.backend049.daos.TransaccionDao;
import com.misiontic.backend049.modelos.Transaccion;

@RestController
@RequestMapping("/api")
public class ControladorTransaccion {
    
    @Autowired
    TransaccionDao transaccionDao;

    @PostMapping("/transacciones")
    public ResponseEntity<Transaccion> guardarTransaccion(@RequestBody Transaccion transaccion){

        Transaccion nuevaTransaccion = transaccionDao.save(transaccion);
        return new ResponseEntity<>(nuevaTransaccion, HttpStatus.CREATED);


    }

    @PutMapping("/transacciones/{id}")
    public ResponseEntity<Transaccion> actualizarTransaccion(@PathVariable("id") long id, @RequestBody Transaccion transaccion){

        Optional<Transaccion> transaccionactualizar = transaccionDao.findById(id);
        if(transaccionactualizar.isPresent()){
             
            Transaccion _transaccion = transaccionactualizar.get();
            
            _transaccion.setComprador((transaccion.getComprador()));
            _transaccion.setVendedor(transaccion.getVendedor());
            _transaccion.setFechaTransaccion(transaccion.getFechaTransaccion());
            _transaccion.setTipoTransaccion(transaccion.getTipoTransaccion());
            _transaccion.setTotal(transaccion.getTotal());

            return new ResponseEntity<>(transaccionDao.save(_transaccion), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    @GetMapping("/transacciones")
    public ResponseEntity<List<Transaccion>> listarTransaccion(){

        List<Transaccion> transaccion = new ArrayList<Transaccion>();
        transaccionDao.findAll().forEach(transaccion::add);

        if(transaccion.isEmpty()){
        return  new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }else{
        return  new ResponseEntity<>(transaccion, HttpStatus.OK);
        }

    }



    @GetMapping("/transacciones/{id}")
    public ResponseEntity<Transaccion> listarTransaccion(@PathVariable("id") long id){

        Optional<Transaccion> transaccion = transaccionDao.findById(id);
        
        if(transaccion.isPresent()){

            return new ResponseEntity<>(transaccion.get(), HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }




    @DeleteMapping("/transacciones/{id}")
    public ResponseEntity<HttpStatus> borrarTransaccion(@PathVariable("id") long id){

        transaccionDao.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }


}
