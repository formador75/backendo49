package com.misiontic.backend049;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Backend049Application {

	public static void main(String[] args) {
		SpringApplication.run(Backend049Application.class, args);
	}

}
