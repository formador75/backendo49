const instancia = axios.create(
    {
        baseURL:"http://localhost:8080/api",
        Headers:{
            "Content-Type": "application/json"
        }
    }
);

async function listarproductos(){

    const respuesta = await instancia.get("/productos");

    let i=0;
    respuesta.data.forEach(function(producto){
        console.log(producto);
        var table = document.getElementById("tablaprueba");
        var row = table.insertRow(i);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);
        i++;
        cell1.innerHTML = producto.nombreProducto;
        cell2.innerHTML = producto.cantidad;
        cell3.innerHTML = producto.precioCompra;
        cell4.innerHTML = producto.precioVenta;
        cell5.innerHTML = '<a class="button" href="/editarproducto.html?id=' + producto.idProducto + '">Editar</a>  | <a class="button" href="/editarproducto.html?id=' + producto.idProducto + '">Borrar</a> ';


    });



}

async function buscarproducto(){

    let nombreproducto= document.getElementById("nombreproducto");
    let cantidad= document.getElementById("cantidad");
    let preciocompra= document.getElementById("preciocompra");
    let precioventa= document.getElementById("precioventa");

    const queryString = window.location.search;
    console.log(queryString);
    const urlParams = new URLSearchParams(queryString);
    const id = urlParams.get('id');
    console.log(id);

    const respuesta = await instancia.get(`/productos/${id}`); 
    nombreproducto.value = respuesta.data.nombreProducto;
    cantidad.value = respuesta.data.cantidad;
    preciocompra.value = respuesta.data.precioCompra;
    precioventa.value = respuesta.data.precioVenta;

}

 
async function guardarproducto(){
    let nombreproducto= document.getElementById("nombreproducto");
    let cantidad= document.getElementById("cantidad");
    let preciocompra= document.getElementById("preciocompra");
    let precioventa= document.getElementById("precioventa");
    let resultado= document.getElementById("resultado");
    try{
    const respuesta = await instancia.post("/productos",{
        nombreProducto:nombreproducto.value,
        cantidad:cantidad.value,
        precioCompra:preciocompra.value,
        precioVenta:precioventa.value
    });
     
    if(respuesta.status == 201){
        nombreproducto.value="";
        cantidad.value="";
        preciocompra.value="";
        precioventa.value="";
        resultado.innerHTML="El producto se guardo con exito";
    }else{
        resultado.innerHTML="El producto NO SE GUARDO";
    }
    }catch(err){
        console.log(err.response.data.error);
        resultado.innerHTML=err.response.data.error;
    }
}

async function editarproducto(){
    let nombreproducto= document.getElementById("nombreproducto");
    let cantidad= document.getElementById("cantidad");
    let preciocompra= document.getElementById("preciocompra");
    let precioventa= document.getElementById("precioventa");
   
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const id = urlParams.get('id');

    try{
    const respuesta = await instancia.put(`/productos/${id}`,{
        nombreProducto:nombreproducto.value,
        cantidad:cantidad.value,
        precioCompra:preciocompra.value,
        precioVenta:precioventa.value
    });
     
    if(respuesta.status == 200){
      
        resultado.innerHTML="El producto se actualizo con exito";
        window.location.reload(true);
    }else{
        resultado.innerHTML="El producto NO SE actualizo";
    }
    }catch(err){
        console.log(err.response.data.error);
        resultado.innerHTML=err.response.data.error;
    }
}
